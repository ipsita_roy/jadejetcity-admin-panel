'use strict';

//All the api url are maintained here
var baseUrl = "http://192.168.0.2:8000/api/v1";
module.exports = {
  login: baseUrl + '/login',
  forgotPassword: baseUrl + '',
  users: baseUrl + '/account/user-list',
  user: baseUrl + '/account/user-list/',
  changePasswordLink: baseUrl + '',
  searchUser: baseUrl + '/account/user-list?searchKey=',
  getProperty: baseUrl + '',
  removeProperty: baseUrl + '',
  services: baseUrl + '/account/services/all',
  getService: baseUrl + '/account/services/service-details/',
  orders: baseUrl + '/account/order/all',
  order: baseUrl + '/account/order/',
  searchOrder: baseUrl + '/account/order/all?searchKey=',
  products: baseUrl + '/account/products/all',
  getProduct: baseUrl + '/account/products/',
  filterOrder: baseUrl + '/account/order/all?filter=',
  changeOrderStatus: baseUrl + '/account/order/update-order/',
  manageServiceRootSequence: baseUrl + '/account/services/manage-route-sequence',
  childServiceManageSequence: function(id) {
    return baseUrl + '/account/services/service-details/' + id + '/manage-sequence';
  },
  productManageSequence: function(id) {
    return baseUrl + '/account/services/service-details/' + id + '/manage-product-sequence';
  }
};
