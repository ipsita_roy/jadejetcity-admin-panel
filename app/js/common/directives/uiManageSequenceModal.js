'use strict';

module.exports = [
  '$uibModal',
  '$rootScope',
  function($uibModal, $rootScope) {
    return {
      transclude: true,
      restrict: 'EA',
      template: '<a href=""class="white-text" ng-click="open()" ng-transclude></a>',
      scope: {
        size: "@",
        list: "=",
        keep: '='
      },
      link: function(scope, element, attrs) {
        scope.template = './resources/templates/manage-sequence-modal-directive.html';
        scope.open = function() {
          var modalInstance = $uibModal.open({
            templateUrl: scope.template,
            controller: 'ManageSequenceModalController',
            controllerAs: 'ManageSequenceModalController',
            size: attrs.size ? attrs.size : '',
            backdrop: attrs.backdrop ? attrs.backdrop : true,
            resolve: {
              modalList: function() {
                return scope.list;
              },
              save: function() {
                return function(data, closeFn) {
                  $rootScope.$broadcast('sortable-element-ids', {
                    data: data,
                    oldData: scope.list,
                    keep: scope.keep,
                    closeFn: closeFn
                  });
                };
              },
              listType: function() {
                return attrs.sequenceType || 'object';
              }
            }
          });

          modalInstance.result.then(function() {
            var manageSequenceModal = document.getElementsByClassName("manage-sequence-modal")[0];
            manageSequenceModal.style.height = manageSequenceModal.offsetHeight;
          }, function() {
            //console.debug('error');
          });
        };
      }
    };

  }
];
