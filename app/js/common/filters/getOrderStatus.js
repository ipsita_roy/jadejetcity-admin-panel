'use strict';

module.exports = [function() {
  return function(status) {
    var orderStatus = '';
    switch (status) {
      case 1:
        orderStatus = 'Pending';
        break;
      case 2:
        orderStatus = 'Rejected';
        break;
      case 3:
        orderStatus = 'Processing';
        break;
      case 4:
        orderStatus = 'Completed';
        break;
    }
    return orderStatus;
  };
}];
