'use strict';

module.exports = [function() {
  return function(value, productOptions) {
    function map(type, index) {
      var returnValue = '';
      switch (productOptions[index].type) {
        case 1:
          returnValue = value[index];
          break;
        case 2:
          returnValue = 'qty.' + value[index];
          break;
        case 3:
          returnValue = value[index] + '.ft';
          break;
      }
      return returnValue;
    }

    return productOptions.reduce(function(returnValue, each, index) {
      if (returnValue.length) {
        returnValue = returnValue + '|' + map(each.type, index);
      } else {
        returnValue = map(each.type, index);
      }
      return returnValue;
    }, '');
  };

}];
