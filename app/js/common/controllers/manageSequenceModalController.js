'use strict';

module.exports = [
  '$scope',
  '$rootScope',
  '$modalInstance',
  'modalList',
  'save',
  'listType',
  // 'UpdateSequenceFactory',
  function($scope, $rootScope, $modalInstance, modalList, save, listType) {

    $scope.uiList = modalList;
    $scope.tmpList = angular.copy(modalList);
    $scope.sortingLog = [];

    $scope.listType = listType === 'object' ? 1 : 0;

    function filterIds(array) {
      var updatedArray = [];
      array.forEach(function(each) {
        if (typeof each === 'object') {
          updatedArray.push(each._id);
        } else { updatedArray.push(each); }
      });
      return updatedArray;
    }

    $scope.sortableOptions = {
      update: function(e, ui) {
        var logEntry = $scope.tmpList.map(function(i) {
          return i;
        }).join(', ');
      },
      stop: function(e, ui) {
        // this callback has the changed model
        var logEntry = $scope.tmpList.map(function(i) {
          return i;
        }).join(', ');
        $scope.updatedList = $scope.tmpList;
      }
    };

    $scope.isHorizontal = false;
    $scope.autoFloating = false;

    $scope.uiFloatingUpdate = function() {
      if ($scope.autoFloating) {
        $scope.sortableOptions['ui-floating'] = 'auto';
      } else {
        $scope.sortableOptions['ui-floating'] = $scope.isHorizontal;
      }
    };

    $scope.flip = function() {
      $scope.isHorizontal = !$scope.isHorizontal;
      $scope.uiFloatingUpdate();
    };
    $scope.flip();

    $scope.cancel = function() {
      $scope.uiList = $scope.arr;
      $modalInstance.dismiss('cancel');
    };

    $scope.saveSequence = function() {
      $scope.cancel();
      return save(filterIds($scope.tmpList));
    }

  }
];
