'use strict';

angular.module('AdminPanel', [
        'ui.router',
        'ui.bootstrap',
        'ui.bootstrap.datetimepicker',
        'angular-ui-validator',
        'toastr',
        'ui.sortable',
        require('./common').name,
        require('./config').name,
        require('./modules').name,
    ])
    .run([
        '$rootScope',
        '$injector',
        'loggedInUser',
        '$state',
        function($rootScope, $injector, loggedInUser, $state) {
            var $state = $injector.get('$state');

            $rootScope.$on('$stateChangeStart', function(event, toState) {
                $rootScope.pageLoading = true;
            });

            $rootScope.$on('$stateChangeSuccess', function() {
                $rootScope.loadingText = '';
                $rootScope.pageLoading = false;
            });

            $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
                $rootScope.pageLoading = false;
            });

            if (!loggedInUser.getData().token) {
                $state.go('login');
            }
        }
    ])
    .config(['toastrConfig', 'AppConfig', '$sceDelegateProvider', function(toastrConfig, AppConfig, $sceDelegateProvider) {
        angular.extend(toastrConfig, AppConfig.toastr);
        $sceDelegateProvider.resourceUrlWhitelist([
            // Allow same origin resource loads.
            'self',
            // Allow loading from our assets domain.  Notice the difference between * and **.
            'https://vendor-apps.s3-us-west-2.amazonaws.com/jade-jet-city/services/**'
        ]);

    }]);
