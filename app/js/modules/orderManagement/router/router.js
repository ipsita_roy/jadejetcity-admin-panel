'use strict';

module.exports = [
  '$stateProvider',
  '$injector',
  '$urlRouterProvider',
  function($stateProvider, $injector, $urlRouterProvider) {
    $stateProvider
      .state('dashboard.orders', {
        url: '/orders',
        template: require('../templates/order-list.html'),
        controller: 'OrderController'
      })
      .state('dashboard.orderDetails', {
        url: '/orders/:id',
        template: require('../templates/order-edit.html'),
        controller: 'OrderDetailsController'
      });
  }
];
