'use strict';

module.exports =
  angular.module('AdminPanel.modules.orderManagement', ['ui.router'])
  .config(require('./router/router'))
  .controller('OrderController', require('./controllers/orderController'))
  .controller('OrderDetailsController', require('./controllers/orderDetailsController'))
  .service('OrdersService', require('./services/ordersService'));
