'use strict';

module.exports = [
  '$scope',
  '$state',
  '$rootScope',
  'OrdersService',
  'loggedInUser',
  'toastr',
  'AppConfig',
  function($scope, $state, $rootScope, OrdersService, loggedInUser, toastr, AppConfig) {

    $scope.orders = [];
    $scope.searchText = {
      value: '',
      orderStatus: '1'
    };


    $scope.loadOrders = function() {
      $rootScope.pageLoading = true;
      OrdersService
        .getOrders()
        .then(function(response) {
          $scope.orders = response.data;
          $scope.orders.filter(function(each) {
            each.totalServices = each.services.length;
            each.totalProducts = 0;
            each.services.filter(function(el) {
              each.totalProducts += el.products;
            });
          });
          console.log('orders', $scope.orders);
        }, function(error) {
          console.log('error', error);
          toastr.error(error, 'Error');
        })
        .finally(function() {
          $rootScope.pageLoading = false;
        });

    };

    $scope.searchOrder = function() {
      $rootScope.pageLoading = true;
      OrdersService
        .searchOrder($scope.searchText.value)
        .then(function(response) {
            $scope.currentPage = 0;
            console.log('Response', response);
            $scope.orders = response.data;
            $scope.orders.filter(function(each) {
              each.totalServices = each.services.length;
              each.totalProducts = 0;
              each.services.filter(function(el) {
                each.totalProducts += el.products;
              });
            });
          },
          function(error) {
            console.log('error', error);
            toastr.error(error, 'Error');
          }).finally(function() {
          $rootScope.pageLoading = false;
        });
    };

    $scope.filterOrder = function() {
      $rootScope.pageLoading = true;
      OrdersService
        .filterOrder(Number($scope.searchText.orderStatus))
        .then(function(response) {
            $scope.currentPage = 0;
            console.log('Response', response);
            $scope.orders = response.data;
            $scope.orders.filter(function(each) {
              each.totalServices = each.services.length;
              each.totalProducts = 0;
              each.services.filter(function(el) {
                each.totalProducts += el.products;
              });
            });
          },
          function(error) {
            console.log('error', error);
            toastr.error(error, 'Error');
          }).finally(function() {
          $rootScope.pageLoading = false;
        });
    };

    $scope.searchUserForNull = function() {
      // console.log("in null ", $scope.searchText.value);
      if (!$scope.searchText.value) {
        $scope.loadOrders();
      }
    };

    $scope.viewOrder = function(order) {
      console.log(order);
      $state.go('dashboard.orderDetails', {
        id: order.orderId
      });
    };

    $scope.currentPage = 0;
    $scope.pageSize = 8;
    $scope.numberOfPages = function() {
      return Math.ceil($scope.users.length / $scope.pageSize);
    };
  }
];
