'use strict';

module.exports = [
  '$scope',
  '$state',
  '$rootScope',
  '$stateParams',
  'OrdersService',
  'loggedInUser',
  'toastr',
  'AppConfig',
  '$filter',
  '$window',
  function($scope, $state, $rootScope, $stateParams, OrdersService, loggedInUser, toastr, AppConfig, $filter, $window) {
    $scope.loadOrder = function() {
      $rootScope.pageLoading = true;
      OrdersService
        .getOrder($stateParams.id)
        .then(function(response) {
          console.log('Order', response);
          $scope.order = response.data;
          $scope.order.orderStatus = $filter('getOrderStatus')($scope.order.status);
          console.log('order status', $scope.order.status);
        }, function(error) {
          status
          console.log('error', error);
          toastr.error(error, 'Error');
        })
        .finally(function() {
          $rootScope.pageLoading = false;
        });
    };

    $scope.acceptOrder = function() {
      $rootScope.pageLoading = true;
      OrdersService
        .changeOrderStatus($stateParams.id, 3)
        .then(function(response) {
          console.log('Order', response);
          $scope.order = response.data;
          $scope.order.orderStatus = $filter('getOrderStatus')($scope.order.status);
          console.log('order status', $scope.order.status);
        }, function(error) {
          console.log('error', error);
          toastr.error(error, 'Error');
        })
        .finally(function() {
          $rootScope.pageLoading = false;
        });
    };

    $scope.rejectOrder = function() {
      $rootScope.pageLoading = true;
      OrdersService
        .changeOrderStatus($stateParams.id, 2)
        .then(function(response) {
          console.log('Order', response);
          $scope.order = response.data;
          $scope.order.orderStatus = $filter('getOrderStatus')($scope.order.status);
          console.log('order status', $scope.order.status);
        }, function(error) {
          console.log('error', error);
          toastr.error(error, 'Error');
        })
        .finally(function() {
          $rootScope.pageLoading = false;
        });
    };

    $scope.completeOrder = function() {
      $rootScope.pageLoading = true;
      OrdersService
        .changeOrderStatus($stateParams.id, 4)
        .then(function(response) {
          console.log('Order', response);
          $scope.order = response.data;
          $scope.order.orderStatus = $filter('getOrderStatus')($scope.order.status);
          console.log('order status', $scope.order.status);
        }, function(error) {
          console.log('error', error);
          toastr.error(error, 'Error');
        })
        .finally(function() {
          $rootScope.pageLoading = false;
        });
    };

    $scope.goBack = function() {
      $window.history.back();
    };
  }
];
