'use strict';

module.exports = [
  '$q',
  'HttpService',
  'API',
  'loggedInUser',
  function($q, HttpService, API, loggedInUser) {
    function getOrders() {
      var deffered = $q.defer();
      var apiObject = {
        url: API.orders,
        token: loggedInUser.getData().token
      };

      HttpService
        .apiRequest(apiObject)
        .then(function(response) {
            deffered.resolve(response);
          },
          function(error) {
            deffered.reject(error);
          });
      return deffered.promise;
    }

    function searchOrder(id) {
      var deffered = $q.defer();
      var apiObject = {
        url: API.searchOrder + id,
        token: loggedInUser.getData().token
      };

      HttpService
        .apiRequest(apiObject)
        .then(function(response) {
            deffered.resolve(response);
          },
          function(error) {
            deffered.reject(error);
          });
      return deffered.promise;
    }

    function filterOrder(status) {
      var deffered = $q.defer();
      var apiObject = {
        url: API.filterOrder + status,
        token: loggedInUser.getData().token
      };

      HttpService
        .apiRequest(apiObject)
        .then(function(response) {
            deffered.resolve(response);
          },
          function(error) {
            deffered.reject(error);
          });
      return deffered.promise;
    }

    function getOrder(id) {
      var deffered = $q.defer();
      var apiObject = {
        url: API.order + id,
        token: loggedInUser.getData().token
      };

      HttpService
        .apiRequest(apiObject)
        .then(function(response) {
            deffered.resolve(response);
          },
          function(error) {
            deffered.reject(error);
          });
      return deffered.promise;
    }

    function changeOrderStatus(id, status) {
      var deffered = $q.defer();

      var apiObject = {
        method: 'PUT',
        url: API.changeOrderStatus + id,
        token: loggedInUser.getData().token,
        data: {
          status: status
        }
      };

      HttpService
        .apiRequest(apiObject)
        .then(function(response) {
            deffered.resolve(response);
          },
          function(error) {
            deffered.reject(error);
          });
      return deffered.promise;
    }



    return {
      getOrders: getOrders,
      searchOrder: searchOrder,
      getOrder: getOrder,
      filterOrder: filterOrder,
      changeOrderStatus: changeOrderStatus
    };
  }
];
