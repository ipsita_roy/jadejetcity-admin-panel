'use strict';

module.exports =
    angular.module('AdminPanel.modules.productManagement', ['ui.router'])
    .config(require('./router/router'))
    .controller('ProductManagementController', require('./controllers/productManagementController'))
    .service('ProductService', require('./services/productService'))
    .controller('ProductDetailsController', require('./controllers/productDetailsController'));
// .controller('PropertyDetailsController', require('./controllers/propertyDetailsController'))
// .factory('PropertyFactory', require('./factories/property-factory'));
