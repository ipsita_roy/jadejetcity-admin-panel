'use strict';

module.exports = [
    '$stateProvider',
    '$injector',
    '$urlRouterProvider',
    function($stateProvider, $injector, $urlRouterProvider) {
        $stateProvider
            .state('dashboard.productManagement', {
                url: '/productManagement',
                template: require('../templates/product-list.html'),
                controller: 'ProductManagementController'
            })
            .state('dashboard.productDetails', {
                url: '/productDetails/:id',
                template: require('../templates/product-edit.html'),
                controller: 'ProductDetailsController'
            });
        // .state('dashboard.propertyDetails', {
        //     url: '/serviceDetails/:serviceId/propertyDetails/:id'
        //         // template: require('../templates/property-edit.html'),
        //         // controller: 'PropertyDetailsController'
        // });
    }
];
