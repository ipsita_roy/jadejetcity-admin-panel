'use strict';

module.exports = [
    '$q',
    'HttpService',
    'API',
    'loggedInUser',
    function($q, HttpService, API, loggedInUser) {
        function getProducts() {
            var deffered = $q.defer();
            var apiObject = {
                url: API.products,
                token: loggedInUser.getData().token
            };

            HttpService
                .apiRequest(apiObject)
                .then(function(response) {
                        deffered.resolve(response);
                    },
                    function(error) {
                        deffered.reject(error);
                    });
            return deffered.promise;
        }

        function getProduct(id) {
            var deffered = $q.defer();
            var apiObject = {
                url: API.getProduct + id,
                token: loggedInUser.getData().token
            }
            HttpService
                .apiRequest(apiObject)
                .then(function(response) {
                        deffered.resolve(response);
                    },
                    function(error) {
                        deffered.reject(error);
                    });
            return deffered.promise;
        }

        return {
            getProducts: getProducts,
            getProduct: getProduct
        };
    }
];
