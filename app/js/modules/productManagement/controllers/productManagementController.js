'use strict';

module.exports = [
    '$scope',
    '$state',
    '$rootScope',
    'ProductService',
    'loggedInUser',
    'toastr',
    'AppConfig',
    function($scope, $state, $rootScope, ProductService, loggedInUser, toastr, AppConfig) {
        $scope.currentPage = 0;
        $scope.pageSize = 20;
        $scope.numberOfPages = function() {
            return Math.ceil($scope.services.length / $scope.pageSize);
        };
        $scope.products=[];

        $scope.loadProducts = function() {
            ProductService.getProducts()
                .then(function(response) {
                        console.log('Response', response);
                        $scope.products = response.data.products;
                    },
                    function(error) {
                        console.log('error', error);
                        toastr.error(error, 'Error');
                    });
        }

        $scope.viewProduct = function(product) {
            $state.go('dashboard.productDetails', {
                id: product._id
            });
        };
    }
];
