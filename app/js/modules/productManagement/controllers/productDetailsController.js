'use strict';

module.exports = [
  '$scope',
  '$state',
  '$rootScope',
  '$stateParams',
  'ProductService',
  'loggedInUser',
  'toastr',
  'AppConfig',
  '$window',
  'PropertyFactory',
  // '$sce',
  function($scope, $state, $rootScope, $stateParams, ProductService, loggedInUser, toastr, AppConfig, $window, PropertyFactory) {
    $scope.scope = $scope;
    $scope.currentPage = 0;
    $scope.pageSize = 8;
    $scope.service = [];
    $scope.numberOfPages = function() {
      return Math.ceil($scope.users.length / $scope.pageSize);
    };

    $scope.loadProduct = function() {
      console.log($stateParams.id);
      // $rootScope.pageLoading = true;
      // $scope.defaultProfilePicture = 'http://files.parsetfss.com/3c31a68b-97c9-46f6-872e-02e75e73a53a/tfss-b86fafbb-fb67-4848-94b6-d28f44fc05ac-default_avatar.png';
      ProductService
        .getProduct($stateParams.id)
        .then(function(response) {
            $scope.currentPage = 0;
            console.log('Response', response);
            // response.data.coverVideo= $sce.getTrustedUrl(response.data.coverVideo);
            $scope.product = angular.copy(response.data);
          },
          function(error) {
            console.log('error', error);
            toastr.error(error, 'Error');
          }).finally(function() {
          $rootScope.pageLoading = false;
        });
    };

    $scope.goBack = function() {
      $window.history.back();
    };

    $scope.modeManager = function(mode) {
      $scope.mode = mode;
      if (mode) {
        $scope._product = angular.copy($scope.product);
      } else {
        $scope.product = angular.copy($scope._product);
      }
      console.log($scope);
    };

    $scope.addNewRange = function(range, data) {
      if (range.map(function(each) {
          return each.toString();
        }).indexOf(data) === -1) {
        range.push(data);
      } else {
        toastr.error('The value already exists. Please add a unique value', 'Error');
      }
    };

    $scope.manageIndex = function(options, data, range) {
      if (data !== null && data !== undefined) {
        options.defaultIndex = data;
      }
      return '' + options.range[options.defaultIndex];
    };

    $scope.removeOptions = function(options, index, $event) {
      if (options.defaultIndex === index) {
        toastr.error('Can not remove default Value', 'Error');
      } else {
        options.range.splice(index, 1);
        if (options.defaultIndex > index) {
          options.defaultIndex -= 1;
        }
      }
      $event.preventDefault();
    };

    $scope.$on('sortable-element-ids', function(ev, cnfg) {
      $scope.product.options[cnfg.keep.$index].range = cnfg.data;
      var oldDefdaultValue =
        cnfg.oldData[$scope.product.options[cnfg.keep.$index].defaultIndex];
      $scope.product.options[cnfg.keep.$index].defaultIndex =
        cnfg.data.reduce(function(returnVal, next, index) {
          if (next === oldDefdaultValue) {
            returnVal = index;
          }
          return returnVal;
        }, null);
    });
  }
];
