'use strict';

module.exports = [
  '$scope',
  '$rootScope',
  '$state',
  '$timeout',
  'loggedInUser',
  'LoginService',
  'toastr',
  'AppConfig',
  function($scope, $rootScope, $state, $timeout, loggedInUser, LoginService, toastr, AppConfig) {

    $scope.moduleClass = 'page-body login-page height-inherit';

    $scope.formData = {
      email: 'sarkar.arijit@innofied.com',
      password: 'innofied123'
    };

    $scope.submitForm = function() {
      $rootScope.pageLoading = true;
      LoginService
        .login($scope.formData)
        .then(function(response) {
            loggedInUser.setData(response.data);
            $state.go('dashboard.userManagement');
            toastr.success(AppConfig.dialog.login.success, 'Welcome ' + response.data.firstName);
          },
          function(error) {
            console.log('error', error);
            toastr.error(AppConfig.dialog.login.failure, 'Error');
          })
        .finally(function() {
          $rootScope.pageLoading = false;
        });
    };
  }
];
