'use strict';
module.exports = [
  '$stateProvider',
  '$urlRouterProvider',
  function($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('login', {
        url: '/login',
        template: require('../templates/login.html'),
        controller: 'LoginController'
      })
      .state('login-forgot', {
        url: '/forgot-password',
        template: require('../templates/forgot-password.html'),
        controller: 'ForgotController'
      })
      .state('dashboard', {
        url: '/dashboard',
        template: require('../templates/leftmenu.html'),
        abstract: true,
        controller: 'LeftMenuController'
      })
      .state('dashboard.editprofile', {
        url: '/editprofile',
        template: require('../templates/edit-profile.html'),
        controller: 'EditProfileController'
      });

    $urlRouterProvider.otherwise('/login');
  }
];
