'use strict';

module.exports = angular.module('AdminPanel.modules', [
  require('./login').name,
  require('./userManagement').name,
  require('./orderManagement').name,
  require('./serviceManagement').name,
  require('./productManagement').name
]);
