'use strict';

module.exports =
    angular.module('AdminPanel.modules.serviceManagement', ['ui.router'])
    .config(require('./router/router'))
    .controller('ServiceManagementController', require('./controllers/serviceManagementController'))
    .service('ServiceService', require('./services/serviceService'))
    .controller('ServiceDetailsController', require('./controllers/serviceDetailsController'));
// .controller('PropertyDetailsController', require('./controllers/propertyDetailsController'))
// .factory('PropertyFactory', require('./factories/property-factory'));
