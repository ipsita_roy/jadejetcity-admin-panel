'use strict';

module.exports = [
    '$q',
    'HttpService',
    'API',
    'loggedInUser',
    function($q, HttpService, API, loggedInUser) {
        function getServices() {
            var deffered = $q.defer();
            var apiObject = {
                url: API.services,
                token: loggedInUser.getData().token
            };

            HttpService
                .apiRequest(apiObject)
                .then(function(response) {
                        deffered.resolve(response);
                    },
                    function(error) {
                        deffered.reject(error);
                    });
            return deffered.promise;
        }

        function getService(id) {
            var deffered = $q.defer();
            var apiObject = {
                url: API.getService + id,
                token: loggedInUser.getData().token
            }
            HttpService
                .apiRequest(apiObject)
                .then(function(response) {
                        deffered.resolve(response);
                    },
                    function(error) {
                        deffered.reject(error);
                    });
            return deffered.promise;
        }

        function manageChildServiceSequence(id, data) {
            var deffered = $q.defer();
            var apiObject = {
                method: "PUT",
                url: API.childServiceManageSequence(id),
                token: loggedInUser.getData().token,
                data: { sequence: data }
            }
            HttpService
                .apiRequest(apiObject)
                .then(function(response) {
                        deffered.resolve(response);
                    },
                    function(error) {
                        deffered.reject(error);
                    });
            return deffered.promise;
        }

        function productManageSequence(id, data) {
            var deffered = $q.defer();
            var apiObject = {
                method: "PUT",
                url: API.productManageSequence(id),
                token: loggedInUser.getData().token,
                data: { sequence: data }
            }
            HttpService
                .apiRequest(apiObject)
                .then(function(response) {
                        deffered.resolve(response);
                    },
                    function(error) {
                        deffered.reject(error);
                    });
            return deffered.promise;
        }

        function manageRootServiceSequence(data) {
            var deffered = $q.defer();
            var apiObject = {
                method: "PUT",
                url: API.manageServiceRootSequence,
                token: loggedInUser.getData().token,
                data: { sequence: data }
            }
            HttpService
                .apiRequest(apiObject)
                .then(function(response) {
                        deffered.resolve(response);
                    },
                    function(error) {
                        deffered.reject(error);
                    });
            return deffered.promise;
        }

        return {
            getServices: getServices,
            getService: getService,
            manageChildServiceSequence: manageChildServiceSequence,
            productManageSequence: productManageSequence,
            manageRootServiceSequence: manageRootServiceSequence
        };
    }
];
