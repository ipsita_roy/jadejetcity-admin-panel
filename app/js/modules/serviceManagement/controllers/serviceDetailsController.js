'use strict';

module.exports = [
    '$scope',
    '$state',
    '$rootScope',
    '$stateParams',
    'ServiceService',
    'loggedInUser',
    'toastr',
    'AppConfig',
    '$window',
    'PropertyFactory',
    // '$sce',
    function($scope, $state, $rootScope, $stateParams, ServiceService, loggedInUser, toastr, AppConfig, $window, PropertyFactory) {

        $scope.currentPage = 0;
        $scope.pageSize = 8;
        $scope.service = [];
        $scope.numberOfPages = function() {
            return Math.ceil($scope.users.length / $scope.pageSize);
        };

        $scope.$on('sortable-element-ids', function(ev, cnfg) {
            console.log('-=-=-=->',cnfg);
            if ($scope.service.children.length && !$scope.service.products) {
                ServiceService.manageChildServiceSequence($scope.service._id, cnfg.data)
                    .then(function(response) {
                            toastr.success(response.message, "Success");
                            cnfg.closeFn();
                        },  
                        function(error) {
                            toastr.error(error, 'Error');
                        });
            } else {
                ServiceService.productManageSequence($scope.service._id, cnfg.data)
                    .then(function(response) {
                            toastr.success(response.message, "Success");
                            cnfg.closeFn();
                        },
                        function(error) {
                            toastr.error(error, 'Error');
                        });
            }
        });

        $scope.loadService = function() {
            ServiceService
                .getService($stateParams.id)
                .then(function(response) {
                        $scope.currentPage = 0;
                        console.log('Response', response);
                        // response.data.coverVideo= $sce.getTrustedUrl(response.data.coverVideo);
                        $scope.service = response.data;
                        $scope.state = ($scope.service.children.length && !$scope.service.products) ? "service" : "product_of_service";
                    },
                    function(error) {
                        console.log('error', error);
                        toastr.error(error, 'Error');
                    }).finally(function() {
                    $rootScope.pageLoading = false;
                });
        };

        $scope.goBack = function() {
            $window.history.back();
        };

        $scope.viewService = function(service) {
            $state.go('dashboard.serviceDetails', {
                id: service.service._id
            });
        };

        $scope.viewProduct = function(product) {
            console.log('------>', product);
            $state.go('dashboard.productDetails', {
                id: product._id
            });
        };
    }
];
