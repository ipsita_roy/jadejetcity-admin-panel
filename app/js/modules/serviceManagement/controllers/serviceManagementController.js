'use strict';

module.exports = [
    '$scope',
    '$state',
    '$rootScope',
    'ServiceService',
    'loggedInUser',
    'toastr',
    'AppConfig',
    function($scope, $state, $rootScope, ServiceService, loggedInUser, toastr, AppConfig) {
        $scope.currentPage = 0;
        $scope.pageSize = 20;
        $scope.numberOfPages = function() {
            return Math.ceil($scope.services.length / $scope.pageSize);
        };
        $scope.services = [];

        $scope.loadServices = function() {
            ServiceService.getServices()
                .then(function(response) {
                        console.log('Response', response);
                        $scope.services = response.data;
                    },
                    function(error) {
                        console.log('error', error);
                        toastr.error(error, 'Error');
                    });
        };

        $scope.viewService = function(service) {
            $state.go('dashboard.serviceDetails', {
                id: service._id
            });
        };

        $scope.$on('sortable-element-ids', function(ev, cnfg) {
            console.log('-------------> ', cnfg);
            ServiceService.manageRootServiceSequence(cnfg.data)
                .then(function(response) {
                        toastr.success(response.message,'Success');
                        cnfg.closeFn();
                    },
                    function(error) {
                        toastr.error(error, 'Error');
                    });
        });
    }
];
