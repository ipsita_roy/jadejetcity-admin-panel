'use strict';

module.exports = [
    '$stateProvider',
    '$injector',
    '$urlRouterProvider',
    function($stateProvider, $injector, $urlRouterProvider) {
        $stateProvider
            .state('dashboard.serviceManagement', {
                url: '/serviceManagement',
                template: require('../templates/service-list.html'),
                controller: 'ServiceManagementController'
            })
            .state('dashboard.serviceDetails', {
                url: '/serviceDetails/:id',
                template: require('../templates/service-edit.html'),
                controller: 'ServiceDetailsController'
            });
        // .state('dashboard.propertyDetails', {
        //     url: '/serviceDetails/:serviceId/propertyDetails/:id'
        //         // template: require('../templates/property-edit.html'),
        //         // controller: 'PropertyDetailsController'
        // });
    }
];
