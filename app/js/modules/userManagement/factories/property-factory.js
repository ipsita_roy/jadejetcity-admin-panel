'use strict';

module.exports = function() {

  var property = {};

  function setProperty(data) {
    property = data;
  }

  function getProperty() {
    // console.log(property);
    return property;
  }

  function resetProperty() {
    property = {};
  }

  return {
    setProperty: setProperty,
    getProperty: getProperty,
    resetProperty: resetProperty
  };

};
