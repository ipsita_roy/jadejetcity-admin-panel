'use strict';

module.exports = [
  '$stateProvider',
  '$injector',
  '$urlRouterProvider',
  function($stateProvider, $injector, $urlRouterProvider) {
    $stateProvider
      .state('dashboard.userManagement', {
        url: '/userManagement',
        template: require('../templates/user-list.html'),
        controller: 'UserManagementController'
      })
      .state('dashboard.userDetails', {
        url: '/userDetails/:id',
        template: require('../templates/user-edit.html'),
        controller: 'UserDetailsController'
      })
      .state('dashboard.propertyDetails', {
        url: '/userDetails/:userId/propertyDetails/:id',
        template: require('../templates/property-edit.html'),
        controller: 'PropertyDetailsController'
      });
  }
];
