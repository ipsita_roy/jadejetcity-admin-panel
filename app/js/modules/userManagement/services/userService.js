'use strict';

module.exports = [
  '$q',
  'HttpService',
  'API',
  'loggedInUser',
  function($q, HttpService, API, loggedInUser) {
    function getUsers() {
      var deffered = $q.defer();
      var apiObject = {
        url: API.users,
        token: loggedInUser.getData().token
      };

      HttpService
        .apiRequest(apiObject)
        .then(function(response) {
            deffered.resolve(response);
          },
          function(error) {
            deffered.reject(error);
          });
      return deffered.promise;
    }

    function searchUser(email) {
      var deffered = $q.defer();
      var apiObject = {
        url: API.searchUser + email,
        token: loggedInUser.getData().token
      };

      HttpService
        .apiRequest(apiObject)
        .then(function(response) {
            deffered.resolve(response);
          },
          function(error) {
            deffered.reject(error);
          });
      return deffered.promise;
    }

    function getUser(id) {
      var deffered = $q.defer();
      var apiObject = {
        url: API.user + id,
        token: loggedInUser.getData().token
      };

      HttpService
        .apiRequest(apiObject)
        .then(function(response) {
            deffered.resolve(response);
          },
          function(error) {
            deffered.reject(error);
          });
      return deffered.promise;
    }

    function changePasswordLink(id) {
      var deffered = $q.defer();
      var apiObject = {
        url: API.changePasswordLink + id,
        token: loggedInUser.getData().token
      };

      HttpService
        .apiRequest(apiObject)
        .then(function(response) {
            deffered.resolve(response);
          },
          function(error) {
            deffered.reject(error);
          });
      return deffered.promise;
    }

    function getProperty(id) {
      var deffered = $q.defer();
      var apiObject = {
        url: API.getProperty + id,
        token: loggedInUser.getData().token
      };

      HttpService
        .apiRequest(apiObject)
        .then(function(response) {
            deffered.resolve(response);
          },
          function(error) {
            deffered.reject(error);
          });
      return deffered.promise;
    }

    function removeProperty(id) {
      var deffered = $q.defer();
      var apiObject = {
        url: API.removeProperty + id,
        token: loggedInUser.getData().token
      };

      HttpService
        .apiRequest(apiObject)
        .then(function(response) {
            deffered.resolve(response);
          },
          function(error) {
            deffered.reject(error);
          });
      return deffered.promise;
    }

    return {
      getUsers: getUsers,
      searchUser: searchUser,
      getUser: getUser,
      changePasswordLink: changePasswordLink,
      getProperty: getProperty,
      removeProperty: removeProperty
    };
  }
];
