'use strict';

module.exports =
  angular.module('AdminPanel.modules.userAccounts', ['ui.router'])
  .config(require('./router/router'))
  .controller('UserManagementController', require('./controllers/userManagementController'))
  .controller('UserDetailsController', require('./controllers/userDetailsController'))
  .controller('PropertyDetailsController', require('./controllers/propertyDetailsController'))
  .service('UserService', require('./services/userService'))
  .factory('PropertyFactory', require('./factories/property-factory'));
