'use strict';

module.exports = [
  '$scope',
  '$state',
  '$rootScope',
  '$stateParams',
  'UserService',
  'loggedInUser',
  'toastr',
  'AppConfig',
  '$window',
  'PropertyFactory',
  function($scope, $state, $rootScope, $stateParams, UserService, loggedInUser, toastr, AppConfig, $window, PropertyFactory) {
    $scope.scope = $scope;
    $scope.currentPage = 0;
    $scope.pageSize = 8;
    $scope.numberOfPages = function() {
      return Math.ceil($scope.users.length / $scope.pageSize);
    };
    $scope.item = {};
    // console.log('inside property controller', PropertyFactory.getProperty());

    $scope.loadProperty = function() {
      $scope.item = PropertyFactory.getProperty();
      console.log($scope.item);
    };


    $scope.goBack = function() {
      $window.history.back();
    };


    // $scope.loadUser = function() {
    //   $rootScope.pageLoading = true;
    //   UserService
    //     .getUser($stateParams.id)
    //     .then(function(response) {
    //         $scope.currentPage = 0;
    //         console.log('Response', response);
    //         $scope.user = response.data;
    //       },
    //       function(error) {
    //         console.log('error', error);
    //         toastr.error(error, 'Error');
    //       }).finally(function() {
    //       $rootScope.pageLoading = false;
    //     });
    // };

    // $scope.sendChangePasswordLink = function() {
    //   $rootScope.pageLoading = true;
    //   UserService
    //     .changePasswordLink($stateParams.id)
    //     .then(function(response) {
    //         $scope.currentPage = 0;
    //         console.log('Response', response);
    //         // $scope.user = response.data;
    //       },
    //       function(error) {
    //         console.log('error', error);
    //         toastr.error(error, 'Error');
    //       }).finally(function() {
    //       $rootScope.pageLoading = false;
    //     });
    // };

  }
];
