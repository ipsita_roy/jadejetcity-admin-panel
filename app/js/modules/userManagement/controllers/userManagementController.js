'use strict';

module.exports = [
  '$scope',
  '$state',
  '$rootScope',
  'UserService',
  'loggedInUser',
  'toastr',
  'AppConfig',
  function($scope, $state, $rootScope, UserService, loggedInUser, toastr, AppConfig) {

    $scope.currentPage = 0;
    $scope.pageSize = 8;
    $scope.numberOfPages = function() {
      return Math.ceil($scope.users.length / $scope.pageSize);
    };

    $scope.users = [];
    $scope.searchText = {
      value: ''
    };

    $scope.loadUsers = function() {
      $rootScope.pageLoading = true;
      UserService
        .getUsers()
        .then(function(response) {
            console.log('Response', response);
            $scope.users = response.data.users;
          },
          function(error) {
            console.log('error', error);
            toastr.error(error, 'Error');
          }).finally(function() {
          $rootScope.pageLoading = false;
        });
    };

    $scope.searchUserForNull = function() {
      // console.log("in null ", $scope.searchText.value);
      if (!$scope.searchText.value) {
        $scope.loadUsers();
      }
    };

    $scope.searchUser = function() {
      $rootScope.pageLoading = true;
      UserService
        .searchUser($scope.searchText.value)
        .then(function(response) {
            $scope.currentPage = 0;
            console.log('Response', response);
            $scope.users = response.data.users;
          },
          function(error) {
            console.log('error', error);
            toastr.error(error, 'Error');
          }).finally(function() {
          $rootScope.pageLoading = false;
        });
    };


    $scope.viewUser = function(user) {
      $state.go('dashboard.userDetails', {
        id: user._id
      });
    };


  }
];
