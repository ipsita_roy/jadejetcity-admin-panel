'use strict';

module.exports = [
  '$scope',
  '$state',
  '$rootScope',
  '$stateParams',
  'UserService',
  'loggedInUser',
  'toastr',
  'AppConfig',
  '$window',
  'PropertyFactory',
  function($scope, $state, $rootScope, $stateParams, UserService, loggedInUser, toastr, AppConfig, $window, PropertyFactory) {
    $scope.scope = $scope;
    $scope.currentPage = 0;
    $scope.pageSize = 8;
    $scope.numberOfPages = function() {
      return Math.ceil($scope.users.length / $scope.pageSize);
    };

    $scope.loadUser = function() {
      $rootScope.pageLoading = true;
      $scope.defaultProfilePicture = 'http://files.parsetfss.com/3c31a68b-97c9-46f6-872e-02e75e73a53a/tfss-b86fafbb-fb67-4848-94b6-d28f44fc05ac-default_avatar.png';
      UserService
        .getUser($stateParams.id)
        .then(function(response) {
            $scope.currentPage = 0;
            console.log('Response', response);
            $scope.user = response.data;
            $scope.user.name = $scope.user.firstName + ' ' + $scope.user.lastName;
          },
          function(error) {
            console.log('error', error);
            toastr.error(error, 'Error');
          }).finally(function() {
          $rootScope.pageLoading = false;
        });
    };

    $scope.sendChangePasswordLink = function() {
      $rootScope.pageLoading = true;
      UserService
        .changePasswordLink($stateParams.id)
        .then(function(response) {
            $scope.currentPage = 0;
            console.log('Response', response);
            // $scope.user = response.data;
          },
          function(error) {
            console.log('error', error);
            toastr.error(error, 'Error');
          }).finally(function() {
          $rootScope.pageLoading = false;
        });
    };

    $scope.goToPropertyManagement = function(item) {
      // console.log(item);
      PropertyFactory.setProperty(item);
      $state.go('dashboard.propertyDetails', {
        userId: $stateParams.id,
        id: item._id
      });

    };

    $scope.goBack = function() {
      $window.history.back();
    };

  }
];
